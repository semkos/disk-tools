#!/bin/bash

echo "[i] Creating GPT partition table"
sudo parted $1 mklabel gpt
sleep 1

echo "[i] Creating primary partition"
sudo parted $1 mkpart primary ext4 0% 100%
sleep 1
