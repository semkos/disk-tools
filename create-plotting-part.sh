#!/bin/bash

./create-ext4-part-full-disk.sh $1

echo "[i] Creating ext4 filesystem (I/O Performance profile)"
sudo mkfs.ext4 -O ^has_journal $1'1'